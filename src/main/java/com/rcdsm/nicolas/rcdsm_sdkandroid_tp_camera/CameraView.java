package com.rcdsm.nicolas.rcdsm_sdkandroid_tp_camera;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Created by Nicolas on 12/11/2014.
 */
public class CameraView extends SurfaceView implements SurfaceHolder.Callback {
	private SurfaceHolder mHolder;
	private Camera mCamera;

	public Camera getmCamera() {
		return mCamera;
	}

	public void setmCamera(Camera mCamera) {
		this.mCamera = mCamera;
	}

	public CameraView(Context context, Camera mCamera) {
		super(context);
		this.mCamera = mCamera;

		this.mHolder = this.getHolder();
		this.mHolder.addCallback(this);
		this.mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		try {
			this.mCamera.setPreviewDisplay(holder);
			mCamera.startPreview();
			Log.i("size", ""+this.getHeight());
			Log.i("size", ""+this.getWidth());

			Camera.Parameters mCameraParameters = this.mCamera.getParameters();
			mCameraParameters.setPreviewSize(this.getWidth(), this.getHeight());
			this.mCamera.setParameters(mCameraParameters);

		} catch (IOException e){
			Log.d("CameraView", "Error setting camera preview: " + e.getMessage());
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		if (mHolder.getSurface() == null){

			return;
		}

		try {
			mCamera.stopPreview();
		} catch (Exception e){

		}

		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();

		} catch (Exception e){
			Log.d("CameraView", "Error starting camera preview: " + e.getMessage());
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

	}
}
