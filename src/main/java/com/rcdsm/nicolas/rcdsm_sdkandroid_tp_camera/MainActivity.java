package com.rcdsm.nicolas.rcdsm_sdkandroid_tp_camera;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.widget.FrameLayout;

public class MainActivity extends Activity {

	private Camera mCamera;
	private CameraView mPreview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mCamera = Camera.open(0);

		mPreview = new CameraView(this, mCamera);

		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);

		preview.addView(mPreview);
	}

	@Override
	protected void onStop() {
		mCamera.release();
		super.onStop();
	}
}
